# Verification process

This project shows the flow of actions in my own verification process. The plan is to use these processes on all my C++ projects to show that the project is memory safe, thread safe, free of undefined behaviour and exhibit high quality code.
## Getting Started

This repo outlines the steps of setting up the build environment, and code verification.

Open the file `compilation_setup_flow.png`, ascertain where you currently are relative to the diagram and then add the missing build configurations to your project.

Open the file `verficiation_flow.png`, start at the far left with the state labelled "programming", follow green arrows on success, follow the red arrows on failure. Ideally you should repeat the process every time a new program feature is added or large code changes have occurred. 

### Prerequisites

The required software is listed in the file named `tools_list.txt`.

## License

This project is licensed under the MIT License - see the license file for details.